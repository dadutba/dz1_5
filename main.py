# -*- coding: utf-8 -*-

import random

random.seed()

# Базовые классы животных:
##########################

class Animal:
    """Класс животное"""    
    name = "Noname"
    voice = "Nonrecognizable"
    
    def __init__(self, name, weight):
        """Инициализация"""        
        self.name = name
        self.weight = weight
    
    def eat(self):
        """Нуждается в еде, покормить"""
        print("Покормили")
    
    def make_sound(self):
        """Ори изо всех сил"""
        return self.voice
    
    def needs(self):
        """Узнать потребности"""       
        out_list = []
        out_list.append("eat")
        return out_list


class Poultry(Animal):
    """Домашняя птичка"""
    
    def lay_eggs(self):
        """Несет яйца. Нужно собирать"""
        print("Собрали яйца")
    
    def needs(self):
        """Узнать потребности"""
        out_list = super().needs()
        out_list.append("lay_eggs")
        return out_list


class Cattle(Animal):
    """Скотинка"""
    pass


class MilkyAnimal(Cattle):
    """Молокодающее животное"""
    
    def give_milk(self):
        """Дает молоко, нужно доить"""
        print("Подоили")
    
    def needs(self):
        """Узнать потребности"""
        out_list = super().needs()
        out_list.append("give_milk")
        return out_list


class FurAnimal(Cattle):
    """Животное, покрытое шерстью"""
    
    def give_fur(self):
        """Покрывается шерстью. Нужно стричь"""
        print("Постригли")
    
    def needs(self):
        """Узнать потребности"""
        out_list = super().needs()
        out_list.append("give_fur")
        return out_list

# Классы конкретных животных:
#############################
        
class Goose(Poultry):
    """Гусь"""
    voice = "Га-га-га"


class Hen(Poultry):
    """Курица"""
    voice = "Ко-ко-ко"


class Cow(MilkyAnimal):
    """Корова"""
    voice = "Муу"


class Sheep(FurAnimal):
    """Овца"""
    voice = "Бее"


class Goat(MilkyAnimal):
    """Коза"""
    voice = "Мее"


class Duck(Poultry):
    """Утка"""
    voice = "Кря-кря"

# Процедуры для работы с животными
##################################
    
def weight_generator():
    return random.uniform(1.5, 155.5)

def audio_recognition(voice):
    """Определить по голосу что за зверь"""
    if voice == "Га-га-га":
        print("- Это же гусь!")
    elif voice == "Ко-ко-ко":
        print("- Это же курица!")
    elif voice == "Муу":
        print("- О, корова!")
    elif voice == "Бее":
        print("- Овца какая-то!")
    elif voice == "Мее":
        print("- Коза!")
    elif voice == "Кря-кря":
        print("- Утка!")

def go_round(in_list=None):
    """Обход животных"""
    print("\nОбойдем животных\n")
    if in_list is None:
        in_list = animal_list
    if not len(in_list):
        print("Животных нет. Всех уже съели ):\n")
        return
    sum_weight = 0
    max_weight = 0
    max_animal = None
    for animal in in_list:
        sum_weight += animal.weight
        if animal.weight > max_weight:
            max_weight = animal.weight
            max_animal = animal
        print("- Скажи что-нибудь.")
        voice = animal.make_sound()
        print("- {}".format(voice))
        audio_recognition(voice)
        print("(Зовут {})".format(animal.name))
        for need in animal.needs():
            getattr(animal, need)()
        print("Вес: {}".format(round(animal.weight, 2)))
        print()
    print("Общий вес животных: {} кг".format(round(sum_weight, 2)))
    print("Самое тяжелое: {} ({} кг)"
          .format(max_animal.name, round(max_animal.weight, 2)))
    audio_recognition(max_animal.voice)
    print()
    
        
# Список домашних животных:
###########################
    
animal_list = [Goose("Серый", weight_generator()),
               Goose("Белый", weight_generator()),
               Cow("Манька", weight_generator()),
               Sheep("Барашек", weight_generator()),
               Sheep("Кудрявый", weight_generator()),
               Hen("Ко-ко", weight_generator()),
               Hen("Кукареку", weight_generator()),
               Goat("Рога", weight_generator()),
               Goat("Копыта", weight_generator()),
               Duck("Кряква", weight_generator())
               ]

# Вызов процедур
################

count = 0
while True:        
    go_round()
    count += 1
    print("\nЗакончен обход {}".format(count))
    user_input = input("Еще раз обойти (q - выход)? ")
    if user_input == "q":
        break
    for animal in animal_list:
        animal.weight = weight_generator()

        

